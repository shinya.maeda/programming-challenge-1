require_relative 'test'

result = Test.new.execute(ARGV[0])
answer = ARGV[1]

if result != answer
  puts "Sorry! Not correct answer!"
  exit 1
end

puts "You got a correct answer! :)"
